import React, { useState } from "react";
import axios from "axios";

const FileUpload = () => {
  const [file, setFile] = useState(null);

  const onChange = (e) => {
    //seteando estado con el archivo
    e.preventDefault();
    console.log(e.target.files[0]);
    setFile(e.target.files[0]);
  };

  const uploadFile = async () => {
    //validar si hay archivo
    if (!file) {
      alert("por favor selecciona un archivo");
      return;
    }

    //pasar archivo a form data
    const data = new FormData();
    const fileData = file;
    data.append("data", fileData);
    console.log(data);
    //peticion http para mandar el form data
    console.log("request htpp");

    try {
      const response = await axios.post({
        method: "post",
        url: "myurl",
        data: data,
        headers: { "Content-Type": "multipart/form-data" },
      });

      console.log(response);
      setFile(null);

      const responseStatus = 200;
      if (responseStatus === 200) {
        alert("el archivo se ha subidco correctamente");
      }
    } catch (error) {
      console.log(error);
      alert(`ha ocurrido un error: ${error.message}`);
    }
    
  };
  return (
    <div>
      <input type="file" onChange={onChange} />
      <br />
      <button onClick={uploadFile}>subir archivo</button>
    </div>
  );
};

export default FileUpload;
