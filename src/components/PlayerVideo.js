import React, { Fragment } from "react";

import { Player } from "video-react";

const PlayerVideo = ({ title, url, poster, styleVideo }) => {
  return (
    <Fragment>
      <div style={styleVideo}>
        <h1>{title}</h1>
        <Player playsInline poster={poster} src={url} />
      </div>
    </Fragment>
  );
};

export default PlayerVideo;
