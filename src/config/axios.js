import axios from "axios";

const axiosClient = axios.createClient({
    baseUrl: "http://localhost:4000/api/"
})

export default axiosClient;