import React, { Fragment } from "react";

import { Row, Col } from "react-flexbox-grid";

import FileUpload from "./components/FileUpload";
import PlayerVideo from "./components/PlayerVideo";

import Padre from "./components/Padre";
import Hijo from "./components/Hijo";

function App() {
  const video = {
    title: "Este es mi espectacular video",
    src: "https://bucketpruebamo.s3.us-east-2.amazonaws.com/Curso.mp4",
    poster: "/assets/poster.png",
  };

  const styleVideo = {
    width: "550px",
    height: "300px",
  };

  return (
    <Row>
      <Col xs={12}>
        <Row center="xs">
          <Col xs={6}>
            <Fragment>
              <FileUpload />
              <PlayerVideo
                title={video.title}
                url={video.src}
                poster={video.poster}
                styleVideo={styleVideo}
              />
            </Fragment>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default App;
